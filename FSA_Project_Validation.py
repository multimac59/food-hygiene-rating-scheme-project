#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-


import FSA_Project

tower_hamlets_file = "Tower_hamlets_FHRS530en-GB.xml"
tower_hamlets_root = get_root(tower_hamlets_file)
tower_hamlets_business_list = get_business(tower_hamlets_root)
tower_hamlets_header_data = get_header_data(tower_hamlets_root)


def validate_clean_int():
  my_check_1 = clean_int("12") == 12
  my_check_2 = clean_int("twelve") is None
  my_check_3 = clean_int(None) is None
  my_check_4 = clean_int(11) == 11
  my_check_5 = clean_int(11.5) == 11
  check_list =  [my_check_1, my_check_2, my_check_3, my_check_4, my_check_5]
  return all(True == check for check in check_list)


def validate_clean_float():
  my_check_1 = clean_float("12") == 12.0
  my_check_2 = clean_float("twelve") is None
  my_check_3 = clean_float(None) is None
  my_check_4 = clean_float(11) == 11.0
  my_check_5 = clean_float(11.5) == 11.5
  check_list =  [my_check_1, my_check_2, my_check_3, my_check_4, my_check_5]
  return all(True == check for check in check_list)
  
  
  

def validate_number_businesses():
  my_check = tower_hamlets_header_data["Item_Count"] == len(tower_hamlets_business_list)
  return my_check

#I think it would be best to completely parse a few business.
#not do below.
def validate_postcodes():
  digits = range(1, 18)
  digits.extend([20, 77, 98])
  list_postcode = ["E" + str(x) for x in digits]
  tower_hamlets_business_postcode = [my_dict["PostCode_Area"] for my_dict in tower_hamlets_business_list]
  tower_hamlets_business_postcode_shorten = list(set(tower_hamlets_business_postcode))
  #tower_hamlets_business_postcode_shorten_sort = tower_hamlets_business_postcode_shorten.sort()
  
  return tower_hamlets_business_postcode_shorten

checked_data = [
  {
                "name": "10 Gales",
                "Address_Line_1": "Railway Arch 10",
                "Address_Line_2": "Gales Gardens",
                "Address_Line_3": "London",
                "PostCode": "E2 0EJ",
                "PostCode_Area": "E2",
                "Rating_Value": "5",
                "Rating_Date": "2010-01-21",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "5",
                "Structural_Score": "5",
                "Confidence_Score": "5",
                "Longitude_Geocode": -0.05670100000000,
                "Latitude_Geocode": 51.52654900000000,
                "Business_Type":"Restaurant/Cafe/Canteen",
                "Business_Type_ID": "1",
                "FHRS_ID": 67298
  },
  {
                "name": "184 Hackney Road",
                "Address_Line_1": None,
                "Address_Line_2": "184 Hackney Road",
                "Address_Line_3": "London",
                "PostCode": "E2 7QL",
                "PostCode_Area": "E2",
                "Rating_Value": "5",
                "Rating_Date": "2014-03-19",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "0",
                "Structural_Score": "0",
                "Confidence_Score": "0",
                "Longitude_Geocode": -0.07278000000000,
                "Latitude_Geocode": 51.53051900000000,
                "Business_Type":"Pub/bar/nightclub",
                "Business_Type_ID": "7843",
                "FHRS_ID": 609755
   },
  {
                "name": "24/7 Off Licence",
                "Address_Line_1": None,
                "Address_Line_2": "141-143 Commercial Road",
                "Address_Line_3": "London",
                "PostCode": "E1 1PX",
                "PostCode_Area": "E1",
                "Rating_Value": "1",
                "Rating_Date": "2013-12-16",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "15",
                "Structural_Score": "10",
                "Confidence_Score": "20",
                "Longitude_Geocode": -0.06237000000000,
                "Latitude_Geocode": 51.51492500000000,
                "Business_Type": "Retailers - other",
                "Business_Type_ID": "4613",
                "FHRS_ID": 78943
   },
  {
                "name": "40 WiNKS Kitchen",
                "Address_Line_1": None,
                "Address_Line_2": "109 Mile End Road",
                "Address_Line_3": "London",
                "PostCode": "E1 4UJ",
                "PostCode_Area": "E1",
                "Rating_Value": "5",
                "Rating_Date": "2013-07-30",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "0",
                "Structural_Score": "0",
                "Confidence_Score": "0",
                "Longitude_Geocode": -0.05058800000000,
                "Latitude_Geocode": 51.52116000000000,
                "Business_Type": "Restaurant/Cafe/Canteen",
                "Business_Type_ID": "1",
                "FHRS_ID": 67894
   }, 
  {
                "name": "5b Urban Bar",
                "Address_Line_1": None,
                "Address_Line_2": "27 Three Colt Street",
                "Address_Line_3": "London",
                "PostCode": "E14 8HH",
                "PostCode_Area": "E14",
                "Rating_Value": "5",
                "Rating_Date": "2014-09-12",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "5",
                "Structural_Score": "5",
                "Confidence_Score": "5",
                "Longitude_Geocode": -0.02977000000000,
                "Latitude_Geocode": 51.51119900000000,
                "Business_Type": "Pub/bar/nightclub",
                "Business_Type_ID": "7843",
                "FHRS_ID": 78297
   }, 
  {
                "name": "5th Avenue",
                "Address_Line_1": "Unit C Arlington Building, Bow Quarter",
                "Address_Line_2": "Fairfield Road",
                "Address_Line_3": "London",
                "PostCode": "E3 2UB",
                "PostCode_Area": "E3",
                "Rating_Value": "4",
                "Rating_Date": "2014-07-16",
                "Local_Authority_Name": "Tower Hamlets",
                "Local_Authority_Code": "530",
                "Hygiene_Score": "5",
                "Structural_Score": "5",
                "Confidence_Score": "10",
                "Longitude_Geocode": -0.02191500000000,
                "Latitude_Geocode": 51.53118300000000,
                "Business_Type": "Retailers - other",
                "Business_Type_ID": "4613",
                "FHRS_ID": 44070
   }
]

def check_certain_th_businessess():
  number_business = len(checked_data)
  return tower_hamlets_business_list[:number_business] == checked_data

