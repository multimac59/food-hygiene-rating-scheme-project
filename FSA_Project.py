#!/usr/bin/env python2.7
# -*- coding:utf-8 -*-

import requests
from bs4 import BeautifulSoup
import time
import xml.etree.ElementTree as ET
import sqlite3
import sys

#set url
url = 'http://ratings.food.gov.uk/open-data/en-GB'
url_score_descriptors = 'http://ratings.food.gov.uk/open-data-resources/lookupData/ScoreDescriptors.xml'

def format_datetime_string(time_string):
  time_tuple = time.strptime(time_string, "%d/%m/%Y at %H:%M")
  reformated_string = time.strftime("%Y-%m-%d %H:%M", time_tuple)
  return reformated_string

def get_text_from_url(link):
  data = requests.get(link)
  return(data.content)

def write_text_to_file(filename, text):
  with open(filename, "w") as file_write:
    file_write.write(text)
  return None

def extract_table_from_source(source, what_table = 2):
  #As of 2015-01-06 there are 12 tables on the webpage.
  #We assume the second list element in python is for London.
  #We set what_table to 2 if the value of the variable is unexpected.
  if what_table != "all" and what_table not in range(12):
    what_table = 2
  soup_data = BeautifulSoup(source)
  data_tables = soup_data.findAll("table")
  returned_table_list = None
  if what_table == "all":
    returned_table_list = data_tables
  else:
    returned_table_list = data_tables[what_table]
    if type(returned_table_list) != list:
      returned_table_list = [returned_table_list]
  return returned_table_list


def table_to_list_dicts(list_tables):
  list_dicts = []
  for table in list_tables:
    headers = table.findAll("th")
    parsed_headers = [txt.text.strip() for txt in headers]
    remaining_table_rows = table.findAll("tr")
    for i, row in enumerate(remaining_table_rows):
      #we exclude the first row since it is the header
      if i != 0:
	row_entries = row.findAll("td")
	row_dict = {}
	for k, row_element in enumerate(row_entries):
	  parsed_element = None
	  if parsed_headers[k] == "Local authority":
	    #we parse and strip text
	    parsed_element = row_element.text.strip()
	  elif parsed_headers[k] == "Last update":
	    #We remove all whitespacing then
	    #we extract the datetime in string format.
	    parsed_element = row_element.text.strip()
	    parsed_element = " ".join(parsed_element.split())
	    parsed_element = format_datetime_string(parsed_element)
	  elif parsed_headers[k] == "Download":
	    #we extract html ref
	    parsed_element = row_element.find("a").get("href")
	  elif parsed_headers[k] == "Number of businesses":
	    parsed_element = row_element.text.strip()
	    parsed_element = int(parsed_element.replace(",",""))
	  row_dict[parsed_headers[k]] = parsed_element
	list_dicts.append(row_dict)
  return list_dicts

def one_dict_borough_download_data(my_dict):
  #we check keys
  if ["Download", "Last update", "Local authority", "Number of businesses"] == sorted(my_dict.keys()):
    download_link = my_dict["Download"]
    download_data = get_text_from_url(download_link)
  else:
    download_data = None
  return download_data


def clean_int(int_string):
  #We attempt to cast string to int
  try:
    cleaned_int = int(int_string)
  except:
    cleaned_int = None
  return cleaned_int


def clean_float(float_string):
  #We attempt to cast the string to float
  try:
    cleaned_float = float(float_string)
  except:
    cleaned_float = None
  return cleaned_float

def get_root(fname):
    tree = ET.parse(fname)
    return tree.getroot()

def attribute_text_set_all(root, path):
  set_attribute = set()
  for my_attribute in root.findall(path):
    if my_attribute is not None:
      my_text = my_attribute.text
      set_attribute.add(my_text)
  return set_attribute




def get_business(root):
    businesses_list = []
    for business in root.findall("./EstablishmentCollection/EstablishmentDetail"):
        data = {
                "name": None,
                "Address_Line_1": None,
                "Address_Line_2": None,
                "Address_Line_3": None,
                "PostCode": None,
                "PostCode_Area":None,
                "Rating_Value": None,
                "Rating_Date": None,
                "Local_Authority_Name": None,
                "Local_Authority_Code": None,
                "Hygiene_Score": None,
                "Structural_Score": None,
                "Confidence_Score": None,
                "Longitude_Geocode": None,
                "Latitude_Geocode": None,
                "Business_Type":None,
                "Business_Type_ID": None,
                "FHRS_ID": None
        }
	#check FHRS ID
	fhrs_id = business.find("./FHRSID")
	if fhrs_id is not None:
	  data["FHRS_ID"] = clean_int(fhrs_id.text)

        #check business name
        business_name = business.find("./BusinessName")
        if business_name is not None:
	  data["name"] = business_name.text
	  
	#check first line address
	address_line_1 = business.find("./AddressLine1")
	if address_line_1 is not None:
	  data["Address_Line_1"] = address_line_1.text
	
	#check second line address
	address_line_2 = business.find("./AddressLine2")
	if address_line_2 is not None:
	  data["Address_Line_2"] = address_line_2.text
	  
	#check third line address
	address_line_3 = business.find("./AddressLine3")
	if address_line_3 is not None:
	  data["Address_Line_3"] = address_line_3.text

	#check postcode
        #extract PostCode string and substring
        postcode_string = business.find("./PostCode")
        if postcode_string is not None:
	  data["PostCode"] = postcode_string.text
	  if type(postcode_string.text) == str:
	    data["PostCode_Area"] = postcode_string.text.split()[0]
	
	#check rating value
	rating_value = business.find("./RatingValue")
	if rating_value is not None:
	  data["Rating_Value"] = rating_value.text
	
	#check rating date
	rating_date = business.find("./RatingDate")
	if rating_date is not None:
	  data["Rating_Date"] = rating_date.text
	
	#check local authority name
	local_authority_name = business.find("./LocalAuthorityName")
	if local_authority_name is not None:
	  data["Local_Authority_Name"] = local_authority_name.text
	
	#check local authority code
	local_authority_code = business.find("./LocalAuthorityCode")
	if local_authority_code is not None:
	  data["Local_Authority_Code"] = local_authority_code.text
	
	#check business type
	business_type = business.find("./BusinessType")
	if business_type is not None:
	  data["Business_Type"] = business_type.text
	
	#check business type id
	business_type_id = business.find("./BusinessTypeID")
	if business_type_id is not None:
	  data["Business_Type_ID"] = business_type_id.text
	
	#Extract the scoredata.
	#Namely the Hygeine, Structural and Confidence In Management Scores
	score = business.find("./Scores")
	if score is not None:
	  #check Hygeine score
	  hygiene = score.find("./Hygiene")
	  if hygiene is not None:
	    data["Hygiene_Score"] = hygiene.text
	  
	  #check Structural Score
	  structural_score = score.find("./Structural")
	  if structural_score is not None:
	    data["Structural_Score"] = structural_score.text
	  
	  #check Confidence In Management Score
	  confidence_mangement_score = score.find("./ConfidenceInManagement")
	  if confidence_mangement_score is not None:
	    data["Confidence_Score"] = confidence_mangement_score.text
        
        #Checks the geocode.
        #Extract the Geocode data.
        #This is the Longitude and Latitude
        geocode = business.find("./Geocode")
        if geocode is not None:
	  #checks the longitude
	  longitude = geocode.find("./Longitude")
	  if longitude is not None:
	    data["Longitude_Geocode"] = clean_float(longitude.text)
	  #checks the latitude
	  latitude = geocode.find("./Latitude")
	  if latitude is not None:
	    data["Latitude_Geocode"] = clean_float(latitude.text)
        
        businesses_list.append(data)

    return businesses_list

def get_header_data(root):
  data = {
    "Extract_Date": None,
    "Item_Count": None
    }
  
  #Find and check header
  header = root.find("./Header")
  if header is not None:
    #Find and check Extract Date
    data_date = header.find("./ExtractDate")
    if data_date is not None:
      data["Extract_Date"] = data_date.text
    #Find and check Item Count
    data_count = header.find("./ItemCount")
    if data_count is not None:
      data["Item_Count"] = clean_int(data_count.text)
  
  return data

def FHRS_score_blurb(score, blurb):
  data_dict = {
    "Hygiene_Score": score,
    "FHRS": blurb
    }
  return data_dict
  
def food_hygiene_rating_score_meaning():
  #This function comes is created using the FHRS stickers on 
  #the following webpage:
  #http://www.food.gov.uk/multimedia/hygiene-rating-schemes/ratings-find-out-more-en
  #
  hygiene_scores_list = [str(x) for x in range(6)]
  blurb_list = [
    "Urgent improvement necessary", 
    "Major improvement necessary",
    "Improvement necessary",
    "Generally satisfactory",
    "Good",
    "Very good"
    ]
  FHRS_list = []
  for score, words in zip(hygiene_scores_list, blurb_list):
    my_dict = FHRS_score_blurb(score, words)
    FHRS_list.append(my_dict)
  return FHRS_list
  
def get_score_descriptors(root):
  #initialise table lists
  confidence_data_list = []
  hygiene_data_list = []
  structural_data_list = []
  #find the score descriptors
  for score_descriptor in root.findall('./{http://schemas.datacontract.org/2004/07/FHRS.Model.Basic}scoreDescriptors/{http://schemas.datacontract.org/2004/07/FHRS.Model.Basic}scoreDescriptors'
):
    score_variable = score_descriptor.find("./{http://schemas.datacontract.org/2004/07/FHRS.Model.Basic}Score")
    score_description = score_descriptor.find("./{http://schemas.datacontract.org/2004/07/FHRS.Model.Basic}Description")
    score_category = score_descriptor.find("./{http://schemas.datacontract.org/2004/07/FHRS.Model.Basic}ScoreCategory")
    descriptor_dict = {
      "Category": None,
      "Description": None,
      "Score": None
      }
    if score_variable is not None:
      descriptor_dict["Score"] = score_variable.text
    if score_description is not None:
      descriptor_dict["Description"] = score_description.text
    if score_category is not None:
      descriptor_dict["Category"] = score_category.text
    
    #we create a new key
    new_key = str(descriptor_dict["Category"]) + "_Score"
    
    #we delete the old category key
    descriptor_dict.pop("Category", None)
    
    #we rename the score key to reflect the category via the new_key
    descriptor_dict[new_key] = descriptor_dict.pop("Score")
    
    if new_key == "Confidence_Score":
      confidence_data_list.append(descriptor_dict)
    if new_key == "Hygiene_Score":
      hygiene_data_list.append(descriptor_dict)
    if new_key == "Structural_Score":
      structural_data_list.append(descriptor_dict)
  
  return confidence_data_list, hygiene_data_list, structural_data_list

  
def list_businessess_to_db(list_dict_business, table_name, filename_db):
  connection = sqlite3.connect(filename_db)
  field_order = ["FHRS_ID", "name", "Address_Line_1", "Address_Line_2", "Address_Line_3", "PostCode", 
		 "PostCode_Area", "Rating_Value", "Rating_Date", "Local_Authority_Name", "Local_Authority_Code", 
		 "Hygiene_Score", "Structural_Score", "Confidence_Score", "Longitude_Geocode", "Latitude_Geocode",
		 "Business_Type", "Business_Type_ID"
		 ]
  businessess_tuples = ()
  for business in list_dict_business:
    business_tuple = ()
    for key in field_order:
      business_tuple = business_tuple + (business[key], )
    businessess_tuples = businessess_tuples + (business_tuple,)
    
  
  with connection:
    cursor = connection.cursor()
    create_query = """CREATE TABLE if not exists """ + str(table_name) + """(FHRS_ID INTEGER PRIMARY KEY,  
      name TEXT, 
      Address_Line_1 TEXT,
      Address_Line_2 TEXT, 
      Address_Line_3 TEXT, 
      PostCode TEXT, 
      PostCode_Area TEXT, 
      Rating_Value TEXT, 
      Rating_Date TEXT, 
      Local_Authority_Name TEXT, 
      Local_Authority_Code TEXT, 
      Hygiene_Score TEXT, 
      Structural_Score TEXT, 
      Confidence_Score TEXT, 
      Longitude_Geocode REAL, 
      Latitude_Geocode REAL, 
      Business_Type TEXT, 
      Business_Type_ID TEXT)"""
    
    
    cursor.execute(create_query)
    
    query_to_execute = """INSERT INTO """ + str(table_name) + """ VALUES(?,?,?,?,?,?,?,?,
      ?,?,?,?,?,?,?,?,?,?)""" 
    
    cursor.executemany(query_to_execute, businessess_tuples)

  return None


if __name__ == "__main__":
  webpage_data = get_text_from_url(url)
  table_data = extract_table_from_source(webpage_data)
  list_dict_urls = table_to_list_dicts(table_data)
  count = 0
  for download_dict in list_dict_urls:
    borough_data = one_dict_borough_download_data(download_dict)
    borough_root = ET.fromstring(borough_data)
    borough_businessess = get_business(borough_root)
    business_apply = list_businessess_to_db(borough_businessess, "London_Businessess", "London_Businessess.db")
    count = count + 1
    print count
    